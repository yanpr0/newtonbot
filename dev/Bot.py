import re
import requests
import telebot
import telegram_api_token


class NewtonBot:
    def __init__(self, token):
        self.token = token
        self.bot = telebot.TeleBot(token)
        self.url = 'https://newton.now.sh'
        self.commands =\
            {"/set_variable":    {"description": "Set value to variable",
                                  "args": ["variable", "value"]},
             "/delete_variable": {"description": "Remove the variable",
                                  "args": ["variable"]},
             "/variables":       {"description": "View all variables",
                                  "args": []},
             "/simplify":        {"description": "Simplify expression",
                                  "args": ["expression"]},
             "/factor":          {"description": "Factorize polynom",
                                  "args": ["polynom"]},
             "/derive":          {"description": "Find derivative",
                                  "args": ["function"]},
             "/integrate":       {"description": "Find integral",
                                  "args": ["function"]},
             "/zeroes":          {"description": "Find function 0's",
                                  "args": ["function"]},
             "/tangent":         {"description": "Find tangent line",
                                  "args": ["function", "point"]},
             "/area":            {"description": "Find area under curve",
                                  "args": ["function", "from", "to"]},
             "/cos":             {"description": "Cosine",
                                  "args": ["x"]},
             "/sin":             {"description": "Sine",
                                  "args": ["x"]},
             "/tan":             {"description": "Tangent",
                                  "args": ["x"]},
             "/arccos":          {"description": "Inverse cosine",
                                  "args": ["x"]},
             "/arcsin":          {"description": "Inverse sine",
                                  "args": ["x"]},
             "/arctan":          {"description": "Inverse tangent",
                                  "args": ["x"]},
             "/abs":             {"description": "Absolute value",
                                  "args": ["value"]},
             "/log":             {"description": "Logarithm",
                                  "args": ["base", "value"]}}

        self.variables = {}

        @self.bot.message_handler(commands=['help', 'start'])
        def show_commands(message):
            reply = "Use these commands to do the math you need:\n"
            for command in self.commands:
                reply += '\n' + command
                reply += " - " + str(self.commands[command]["args"])
                reply += " - " + self.commands[command]["description"]
            reply += "\n\nUse @<variable> to refer to a variable"
            reply += "\nDon't use spaces in each separate expression"
            reply += " and use spaces between them"
            reply += "\nDon't skip * between multipliers"
            self.bot.send_message(message.chat.id, reply)

        @self.bot.message_handler(commands=['set_variable'])
        def set_variable(message):
            data = message.text.split()
            if len(data) != 1 + len(self.commands['/set_variable']['args']):
                self.argument_error(message.chat.id)
                return
            try:
                data[2] = self.prepare_for_url(message.chat.id, data[2])[0]
            except NameError:
                self.variable_error(message.chat.id)
                return
            self.variables[message.chat.id] = self.variables.get(
                message.chat.id, {})
            self.variables[message.chat.id][data[1]] = data[2]

        @self.bot.message_handler(commands=['delete_variable'])
        def delete_variable(message):
            data = message.text.split()
            if len(data) != 1 + len(self.commands['/delete_variable']['args']):
                self.argument_error(message.chat.id)
                return
            self.variables[message.chat.id] = self.variables.get(
                message.chat.id, {})
            if data[1] not in self.variables[message.chat.id]:
                self.variable_error(message.chat.id)
                return
            self.variables[message.chat.id].pop(data[1])

        @self.bot.message_handler(commands=['factor', 'derive', 'integrate',
                                            'zeroes', 'cos', 'sin', 'tan',
                                            'arccos', 'arcsin', 'arctan',
                                            'abs', 'simplify'])
        def proceed_commands(message):
            data = message.text.split()
            if len(data) != 1 + len(self.commands[data[0]]['args']):
                self.argument_error(message.chat.id)
                return
            command, arg = message.text.split()
            try:
                arg = self.prepare_for_url(message.chat.id, arg)[0]
            except NameError:
                self.variable_error(message.chat.id)
                return
            response = requests.get(self.url + command + '/' + arg).json()
            self.bot.send_message(message.chat.id, response['result'])

        @self.bot.message_handler(commands=['tangent'])
        def get_tangent_line(message):
            data = message.text.split()
            if len(data) != 1 + len(self.commands['/tangent']['args']):
                self.argument_error(message.chat.id)
                return
            try:
                function, point = self.prepare_for_url(
                    message.chat.id, *message.text.split()[1:])
            except NameError:
                self.variable_error(message.chat.id)
                return
            response = requests.get(
                self.url + '/tangent/' + point + '|' + function).json()
            self.bot.send_message(message.chat.id, response['result'])

        @self.bot.message_handler(commands=['area'])
        def get_area(message):
            data = message.text.split()
            if len(data) != 1 + len(self.commands['/area']['args']):
                self.argument_error(message.chat.id)
                return
            try:
                function, a, b = self.prepare_for_url(
                    message.chat.id, *message.text.split(' ', 3)[1:])
            except NameError:
                self.variable_error(message.chat.id)
                return
            response = requests.get(
                self.url + '/area/' + a + ':' + b + '|' + function).json()
            self.bot.send_message(message.chat.id, response['result'])

        @self.bot.message_handler(commands=['log'])
        def log(message):
            data = message.text.split()
            if len(data) != 1 + len(self.commands['/log']['args']):
                self.argument_error(message.chat.id)
                return
            try:
                base, arg = self.prepare_for_url(
                    message.chat.id, *message.text.split(' ', 2)[1:])
            except NameError:
                self.variable_error(message.chat.id)
                return
            response = requests.get(
                self.url + '/log/' + base + '|' + arg).json()
            self.bot.send_message(message.chat.id, response['result'])

        @self.bot.message_handler(commands=['variables'])
        def view_variables(message):
            data = message.text.split()
            if len(data) != 1 + len(self.commands['/variables']['args']):
                self.argument_error(message.chat.id)
                return
            reply = "Current variables:\n"
            self.variables[message.chat.id] = self.variables.get(
                message.chat.id, {})
            if len(self.variables[message.chat.id]) == 0:
                reply += "None of them"
            for variable in self.variables[message.chat.id]:
                reply += '\n' + variable + " = " +\
                    self.variables[message.chat.id][variable]
            self.bot.send_message(message.chat.id, reply)

        @self.bot.message_handler()
        def proceed_other(message):
            self.bot.send_message(
                message.chat.id,
                "Sorry, I can't understand you...\nTry /help")

    def argument_error(self, chat_id):
        self.bot.send_message(
            chat_id,
            "Wrong number of arguments was provided!\n" +
            "Last operation was canceled\nTry /help")

    def variable_error(self, chat_id):
        self.bot.send_message(
            chat_id,
            "No such variable existing!\n" +
            "Last operation was canceled\nTry /variables")

    def prepare_for_url(self, chat_id, *expressions):
        new = list(expressions)
        for i in range(len(expressions)):
            new[i] = new[i].replace('/', '(over)')
            match = re.search('@[a-zA-Z_]+', new[i])
            while match:
                if match[0][1:] not in self.variables[chat_id]:
                    raise NameError
                new[i] = new[i].replace(
                    match[0], self.variables[chat_id][match[0][1:]], 1)
                match = re.search('@[a-zA-Z_]+', new[i])
            return new

    def run(self):
        self.bot.polling()


if __name__ == '__main__':
    NewtonBot(telegram_api_token.token).run()
